#encoding=utf-8
'''
Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html
'''

import MySQLdb
import datetime
import time
import Scrapper

class Track:
    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.coords = []
        self.complete = 0
        self.tracking = None
        
class Train:
    def __init__(self, guid, track):
        self.guid=guid
        self.track=track
        self.trackedfromstart = 0

class TrainTracker:
    def __init__(self):
        self.trains = []
        emptytrack = Track(None,None)
        emptytrack.complete = 1
        self.tracks = [emptytrack]
    
    def addTrack(self, track):
        self.tracks += [track]
        
    def addTrain(self, train):
        self.trains += [train]
        
    def getTrain(self, guid):
        l = [t for t in self.trains if t.guid == guid]
        if len(l)==0:
            train = Train(guid, None)
            self.trains += [train]
            return train
        return l[0]
    
    def getTrack(self, start, end):
        l = [t for t in self.tracks if t.start == start and t.end == end]
        if len(l)==0:
            track = Track(start, end)
            self.tracks += [track]
            return track
        return l[0]
        
    def updateTrain(self, guid, georss, start, end):
        train = self.getTrain(guid)
        track = train.track
        if track == None:
            track = Track(start, end)
            train.track = track
            train.trackedfromstart = 0
            return
        
        if track.start == start and track.end == end:
            if train.trackedfromstart == 1 and track.complete == 0 and track.tracking == train.guid and georss[0]>1 and georss[1]>1:
                #tracking in progress and georss data not (0,0) -> add coordinate
                track.coords += [georss]
        else:
            # current track is different from previous
            # complete previous track
            if train.trackedfromstart == 1:
                track.complete = 1
            # initiate tracking for new track if track is not in progress of tracking another train
            track = self.getTrack(start, end)
            if track.tracking == None and georss[0]>1 and georss[1]>1:
                track.tracking = train.guid
                track.coords += [georss]
                train.trackedfromstart = 1
                train.track = track
            
class Analysis:
    def __init__(self):
        self.tracker = TrainTracker()
        self.unsavedObservations = []
        
    def analyze(self, trainObservations):
        '''
        trainObservations - List of TrainObservation -objects
        '''
        self.unsavedObservations += trainObservations
        for tObs in trainObservations:
            self.tracker.updateTrain(tObs.guid, tObs.georss, tObs.previousStation, tObs.nextStation)
          
    def getTrackId(self, cur, previousStation, nextStation):
        '''
        Searches database using cursor cur for a track previousStation - nextStation
        If such an item does not exist, it is created
        Returns the id of the track  
        '''
        cur.execute(u"SELECT idTrack FROM Track WHERE startStation='{0}' AND endStation='{1}'".format(previousStation,nextStation))
        rows = cur.fetchall()
        if len(rows)==0:
            # Track obs.previousStation -> obs.nextStation does not exist
            # check if both stations exist in station table
            cur.execute(u"SELECT * FROM Station WHERE stationCode='{0}'".format(previousStation))
            r = cur.fetchall()
            if len(r)==0:
                # create previousStation entry
                stationcode = previousStation.encode('utf-8')
                stationname, georss = Scrapper.getStation(stationcode, Scrapper.trainStationXml)
                geolat,geolong = georss
                cur.execute(u"INSERT INTO Station VALUES ('{0}','{1}',{2:.5f},{3:.5f})".format(previousStation, stationname, geolat, geolong))
            cur.execute(u"SELECT * FROM Station WHERE stationCode='{0}'".format(nextStation))
            r = cur.fetchall()
            if len(r)==0:
                # create nextStation entry
                stationcode = nextStation.encode('utf-8')
                stationname, georss = Scrapper.getStation(stationcode, Scrapper.trainStationXml)
                geolat,geolong = georss
                cur.execute(u"INSERT INTO Station VALUES ('{0}','{1}',{2:.5f},{3:.5f})".format(nextStation, stationname, geolat, geolong))
                
            # insert new track entry into database
            cur.execute(u"INSERT INTO Track (startStation, endStation) VALUES ('{0}','{1}')".format(previousStation,nextStation))
            cur.execute(u"SELECT idTrack FROM Track WHERE startStation='{0}' AND endStation='{1}'".format(previousStation,nextStation))
            rows = cur.fetchall()
            
        trackid = int(rows[0][0])
        return trackid
            
    def saveToDatabase(self):
        '''
        Saves all unsaved entries into the local database
        Returns the number of unsaved entries remaining
        '''
        database = MySQLdb.connect(host = "127.0.0.1", user="vexpi", passwd="vexpi", db="vexpi")
        cur = database.cursor()
        # TODO: is it better to keep the same cursor for writing all data, or instead open and close a new cursor every time an entry is saved?
        
        try:
            # encapsulate all database operations in one try - except
            while len(self.unsavedObservations)>0:
                obs = self.unsavedObservations.pop(0)
                if obs.previousStation==None or obs.nextStation == None:
                    continue
                geoLat,geoLong = obs.georss
                trackid = self.getTrackId(cur, obs.previousStation, obs.nextStation)
    
                try:
                    sqlstr = "INSERT INTO Trainobservation VALUES ('{0}','{1}',{2},'{3}',{4},{5:.5f},{6:.5f},{7})".format(obs.guid,obs.pubDate,obs.speed,obs.reasonCode or "",obs.lateness,geoLat,geoLong,trackid)
                    cur.execute(sqlstr)
                except MySQLdb.Error, e:
                    print e
                    # check if data in db
                    cur.execute("SELECT * FROM Trainobservation WHERE guid='{0}' AND pubDate='{1}'".format(obs.guid,obs.pubDate))
                    res = cur.fetchall()
                    if len(res)==0:
                        # saving to database failed, add item back to the unsaved list and abort saving the rest for now
                        self.unsavedObservations.append(obs)
                        database.commit()
                        cur.close()
                        database.close()
                        return len(self.unsavedObservations)
                
            completeTracks = [t for t in self.tracker.tracks if t.complete == 1 and t.start != None and t.end != None]
            
            for track in completeTracks:
                # search db for track 'track.start - track.end', and save route data if not already in database
                trackid = self.getTrackId(cur, track.start, track.end)
                cur.execute("SELECT * FROM Route WHERE track={0}".format(trackid))
                rows = cur.fetchall()
                if len(rows)==0:
                    # no route data saved for track, save now
                    # get coordinates for route start/end
                    cur.execute("SELECT * FROM Track WHERE idTrack={0}".format(trackid))
                    trackrow = cur.fetchall()[0]
                    startstation = trackrow[1]
                    endstation = trackrow[2]
                    
                    # add route start coordinate
                    stationcode = startstation.decode('unicode_escape').encode('utf-8')
                    stationname, georss = Scrapper.getStation(stationcode, Scrapper.trainStationXml)
                    geoLat, geoLong = georss
                    cur.execute("INSERT INTO Route VALUES ({0},{1},{2:.5f},{3:.5f})".format(trackid,0,geoLat,geoLong))
                    curindex=1
                    
                    for i in range(len(track.coords)):
                        if (track.coords[i][0]!=geoLat or track.coords[i][1]!=geoLong):
                            geoLat, geoLong = track.coords[i]
                            cur.execute("INSERT INTO Route VALUES ({0},{1},{2:.5f},{3:.5f})".format(trackid,curindex,geoLat,geoLong))
                            curindex+=1
                            
                    # add route end coordinate
                    stationcode = endstation.decode('unicode_escape').encode('utf-8')
                    stationname, georss = Scrapper.getStation(stationcode, Scrapper.trainStationXml)
                    geoLat, geoLong = georss
                    cur.execute("INSERT INTO Route VALUES ({0},{1},{2:.5f},{3:.5f})".format(trackid,curindex,geoLat,geoLong))
                    
                else:
                    # remove unnecessary route data from traintracker
                    track.coords = []
        except MySQLdb.Error, e:
            print e   
             
        database.commit()
        cur.close()
        database.close()
        return len(self.unsavedObservations)