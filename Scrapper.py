#encoding=utf-8
'''
Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkil� Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html
'''

import urllib
import re
import copy
from BeautifulSoup import BeautifulStoneSoup
from datetime import datetime, date, time



trainStationXml = "http://188.117.35.14/TrainRSS/TrainService.svc/stationInfo?station=" 
trainXml = "http://188.117.35.14/TrainRSS/TrainService.svc/trainInfo?train="
trainListXml = "http://188.117.35.14/TrainRSS/TrainService.svc/AllTrains"

nore = None
error = "error"
timeZone = +0300

class TrainObservation:
    def __init__(self, guid, previousStation, nextStation, pubDate, lateness, georss, reasonCode, speed):
        self.guid = guid                    
        self.previousStation = previousStation         
        self.nextStation = nextStation
        self.pubDate = pubDate                 
        self.lateness = int(lateness)
        self.georss = georss                  
        self.reasonCode = reasonCode
        self.speed = int(speed)
    
    
'''
Soupping function to change the XML data to Soup -object.
Soup object is class of Beautifulsoup -libraries, and 
the Beautifulsoup -libraries has been developed to parse 
XML -data.
@param data 
@return soup
'''
def soupping (data):
    soup = BeautifulStoneSoup(data, selfClosingTags=['reasonCode'])
    return soup


'''
Function getTrain search train information from Vr web-sites with the guid.
Guid is the symbol of the train. Moreover, the train information is soupping.
@param guid
@return soup
'''
def getTrain (guid, addres):
    train = getData(addres + guid)
    if train == error:
       return 
    soup = soupping(train)
    return soup


'''
Function getData srape data from web -sites.
@param addres
@return data
'''
def getData(addres):
    try: 
       url = urllib.urlopen(addres)
       data = url.read()
       url.close() 
    except IOError:
       return error
    return data       


'''
Function getTrainData search the trains information with function
getData and Beautifoulsoup -libraries, compresses data to object
and return list of the object.
@param guid
@param tila
@return 1;  Not used
@create list trainObservation

'''
def getTrainData(guid, addres, tila):
  try:    
    data = getTrain(guid, addres)
    trainguid = data.trainguid.contents[0]
    speed = data.speed.contents[0]
    reason = data.reasoncode.contents[0]
    late = data.lateness.contents[0]
    
    if reason == data.lateness:
       reason = nore
   
    pub = data.lastbuilddate.contents[0]
    pub1 = pub[:pub.rfind('+')].strip()
    pub2 = pub[pub.rfind('+'):].strip("()")
    
    if tila == 1:
        timeZone = pub2
    
    pubO = datetime.strptime(pub1, "%a, %d %b %Y %H:%M:%S")
    georssstr = data.find('georss:point').string
    coords = georssstr.split()
    georss = float(coords[0]), float(coords[1])
    
    previousStation = None
    nextStation = None
    
    for item in data.findAll('item'):
        next = item.nextSibling
        #etd  = item.etd.contents[0]
        #print etd
        #etdt = time.strftime(etd)
        #etd1 = etd[:etd.rfind(':')].strip()
        #etd2 = etd[etd.rfind(':'):].strip("()")
        
        #etdO = copy.copy(pubO)
        #etdO.replace(hour = 1)
        #etdO = timedelta(etd)
        
        #etdO = pubO
        #etdO = time.strftime(etd, "%H:%M:%S")
        #etdO = pubO.(etd, "%H:%M") 
        #print etd
        if  (int(item.completed.contents[0]) == 1 and (next == None or  int(next.completed.contents[0]) == 0)):
            previousStation = item.stationcode.contents[0]
            if next != None:
               nextStation = next.stationcode.contents[0]
            else: 
               nextStation = None   
    #trainObservation = []      
    trainObs = TrainObservation(trainguid, previousStation, nextStation, pubO, late, georss, reason, speed)
    #trainObservation.append(trainObs)
    return trainObs
  except (AttributeError, TypeError):    
    return None

def getStation(station, trainStationXml):
    station = getTrain(station, trainStationXml)
    stationName = station.title.contents[0]
    georssstr = station.find('georss:point').string
    coords = georssstr.split()
    georss = float(coords[0]), float(coords[1])
    station = stationName, georss
    return station




def Scrape():
   trains = getData(trainListXml)
   soup = soupping(trains)
   tila = 1
   #trainList = []
   trainObservation = []
   for item in soup.findAll('item'):
      if item.category.string == '1':
         guid = item.guid.renderContents()
         trainO = getTrainData(guid, trainXml, tila)
         if trainO is not None:
            trainObservation.append(trainO)
   #tila = 0
         #trainList.append(guid)
   return trainObservation
          



'''        
Main -function for testing.

'''



if __name__ == "__main__":
   lap = 1
   while 1:
    trainObservation = Scrape()
    lap = lap+1
    '''
    for item in trainObservation:
        print item.guid
        print item.previousStation
        print item.nextStation
     '''   
    print lap    
    print "loppu"      

'''
    trains = getData(trainListXml)
    soup = soupping(trains)
    tila = 1
    trainList = []
    trainObservation = []
    for item in soup.findAll('item'):
      if item.category.string == '1':
         guid = item.guid.renderContents()
         ret = getTrainData(guid, trainXml, tila)
         tila = 0
         trainList.append(guid)


    
   


for item in trainObservation:
        print item.guid
        print item.previousStation
        print item.nextStation
print "loppu"               
'''
       
       
              








   
    
    
    
