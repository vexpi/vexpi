#encoding=utf-8
'''
Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkil� Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html
'''
import datetime
import MySQLdb
import numpy
import calendar

def dateFromISOcalendar(day,week,year):
    '''
    Calculates the standard date from a ISO week date, i.e. converts 
    2,16,2013 = Day 2 of week number 16, year 2013
    into
    12. April 2013 
    Returns a datetime.date object of the converted date
    '''
    jan4 = datetime.date(year, 1, 4)
    return jan4 + datetime.timedelta(weeks = week-1, days = day-jan4.isoweekday())

newpasstimedelta = datetime.timedelta(hours=3) #TODO: remove dependence on arbitrary time interval

class TrainTrackPass:
    def __init__(self):
        self.latestDate = None
        self.lateness=0
        self.speedData=[]
    def addData(self, pubDate, lateness, speed):
        self.speedData.append(speed)
        if self.latestDate==None or pubDate > self.latestDate:
            self.latestDate=pubDate
            self.lateness = lateness

class TrainTrackData:
    def __init__(self, guid, trackid):
        self.guid=guid
        self.trackid=trackid
        self.passes=[]
    def addData(self, pubDate, lateness, speed):
        lst = [p for p in self.passes if pubDate - p.latestDate < newpasstimedelta]
        if len(lst)>0:
            lastpass = lst[0]
            if lastpass.latestDate == None or pubDate - lastpass.latestDate < newpasstimedelta:
                # append to current pass
                lastpass.addData(pubDate, lateness,speed)
            else:
                # create new pass
                newpass = TrainTrackPass()
                newpass.addData(pubDate, lateness, speed)
                self.passes.append(newpass)
        else:
            # create new pass
            newpass = TrainTrackPass()
            newpass.addData(pubDate, lateness, speed)
            self.passes.append(newpass)
    def getLateness(self):
        obscount = len(self.passes)
        latenessData = [ttp.lateness for ttp in self.passes]
        avg = numpy.average(latenessData)
        median = numpy.median(latenessData)
        max = numpy.max(latenessData)
        return (obscount, avg, median, max)
    def getSpeed(self):
        speedData = []
        for ttp in self.passes:
            speedData += ttp.speedData
        obscount = len(speedData)
        avg = numpy.average(speedData)
        median = numpy.median(speedData)
        max = numpy.max(speedData)
        return (obscount, avg, median, max)

def calculateAllTime(updatedDate):
    '''
    Updates table Analyzeddata_alltime with the data from all entries in Trainobservation,
    where Trainobservation.pubDate is after the given 'updatedDate', i.e. the last time a data update occurred.
    '''
    database = MySQLdb.connect(host = "127.0.0.1", user="vexpi", passwd="vexpi", db="vexpi")
    cur = database.cursor()
    
    cur.execute("SELECT * FROM Trainobservation WHERE pubDate > '{0}'".format(updatedDate))
    rows = cur.fetchall()
    
    if len(rows)==0:
        #nothing to update, return
        cur.close()
        database.close()
        return
    
    # process observation data into traintrackdata objects
    ttdata = []
    for r in rows:
        guid = r[0]
        pubDate = r[1]
        speed = r[2]
        lateness = r[4]
        trackid = r[7]
        
        l = [t for t in ttdata if t.guid==guid and t.trackid == trackid]
        if len(l)==0:
            # add new traintrackdata object
            t = TrainTrackData(guid, trackid)
            t.addData(pubDate, lateness, speed)
            ttdata.append(t)
        else:
            t=l[0]
            t.addData(pubDate, lateness, speed)
    
    # collect processed data and save into database
    for t in ttdata:
        (obsc, lateavg, latemedian, latemax) = t.getLateness()
        (obscspd, spdavg, spdmedian, spdmax) = t.getSpeed()
        
        # database might contain partial data, check database for existing entry
        cur.execute("SELECT * FROM Analyzeddata_alltime WHERE guid='{0}' AND track={1}".format(t.guid,t.trackid))
        entries = cur.fetchall()
        if len(entries)>0:
            savedObsCount, slAvg, slMax, sspdAvg, sspdMax = entries[0][2:7]
            # update data entry
            newCount = savedObsCount+obsc
            lateavg = (lateavg*obsc + slAvg*savedObsCount)/float(newCount)
            if slMax>latemax:
                latemax = slMax
            spdavg = (spdavg*obsc + sspdAvg*savedObsCount)/float(newCount)
            if sspdMax > spdmax:
                spdmax=sspdMax
            
            sqlstr = """UPDATE Analyzeddata_alltime
                        SET observationCount={0}, latenessAverage={1}, latenessMax={2}, speedAverage={3}, speedMax={4}
                        WHERE guid='{5}' AND track={6}""".format(newCount, lateavg,latemax,spdavg,spdmax,t.guid,t.trackid)
            cur.execute(sqlstr)
        else:
            # create new data entry
            sqlstr = "INSERT INTO Analyzeddata_alltime VALUES ('{0}',{1},{2},{3},{4},{5},{6})".format(t.guid, t.trackid, obsc, lateavg,latemax,spdavg,spdmax)
            cur.execute(sqlstr)
    
    cur.close()
    database.commit()
    database.close()

def calculateWeekOfDate(date):
    '''
    Updates table Analyzeddata_week with the data from all entries in the table Trainobservation corresponding to the given week.
    If there exist entries corresponding to the week in Trainobservation, then all previous data for the week in Analyzeddata_week will be deleted.
    If no entries for the week exist in Trainobservation, nothing is done.
    date - a datetime.date object that is a part of the week to be processed. Weekday doesn't matter, that is, any day Monday-Sunday will function the same.
    '''
    year, week, day = date.isocalendar()
    calculateWeek(year,week)

def calculateWeek(year, week):
    '''
    Updates table Analyzeddata_week with the data from all entries in the table Trainobservation corresponding to the given week.
    If there exist entries corresponding to the week in Trainobservation, then all previous data for the week in Analyzeddata_week will be deleted.
    If no entries for the week exist in Trainobservation, nothing is done.
    year - ISO year of the week
    week - ISO weeknumber
    '''
    firstDayOfWeek = dateFromISOcalendar(1, week, year)
    nextWeek = firstDayOfWeek + datetime.timedelta(weeks=1)
    
    database = MySQLdb.connect(host = "127.0.0.1", user="vexpi", passwd="vexpi", db="vexpi")
    cur = database.cursor()
    
    cur.execute("SELECT * FROM Trainobservation WHERE pubDate BETWEEN '{0}' AND '{1}'".format(firstDayOfWeek,nextWeek))
    rows = cur.fetchall()
    if len(rows)==0:
        # no data for the week exists
        cur.close()
        database.close()
        return
    
    # delete pre-existing data, if any
    cur.execute("DELETE FROM Analyzeddata_week WHERE week={0} AND year={1}".format(week,year))
    
    # process observation data into traintrackdata objects
    ttdata = []
    for r in rows:
        guid = r[0]
        pubDate = r[1]
        speed = r[2]
        lateness = r[4]
        trackid = r[7]
        
        l = [t for t in ttdata if t.guid==guid and t.trackid == trackid]
        if len(l)==0:
            # add new traintrackdata object
            t = TrainTrackData(guid, trackid)
            t.addData(pubDate, lateness, speed)
            ttdata.append(t)
        else:
            t=l[0]
            t.addData(pubDate, lateness, speed)
        
    # collect processed data and save into database
    for t in ttdata:
        (obsc, lateavg, latemedian, latemax) = t.getLateness()
        (obscspd, spdavg, spdmedian, spdmax) = t.getSpeed()
        sqlstr = "INSERT INTO Analyzeddata_week VALUES ('{0}',{1},{2},{3},{4},{5},{6},{7},{8},{9},{10})".format(t.guid, t.trackid, week, year, obsc, lateavg,latemedian,latemax,spdavg,spdmedian,spdmax)
        cur.execute(sqlstr)
    
    cur.close()
    database.commit()
    database.close()

def calculateMonth(year, month, updatedDate):
    '''
    Updates table Analyzeddata_month with the data from all entries in Trainobservation,
    where Trainobservation.pubDate is after the given 'updatedDate', i.e. the last time a data update occurred.
    '''
    if month<12:
        dateEnd = datetime.datetime(year, month+1, 1)
    else:
        dateEnd = datetime.datetime(year+1,month,1)
        
    if updatedDate > dateEnd:
        # data updated after month is over -> all data is already saved
        return
    
    partialdata = 0
    dateBegin = datetime.datetime(year,month,1)
    if updatedDate > dateBegin:
        # data updated after beginning of the month -> partial data exists in database
        dateBegin = updatedDate
        partialdata = 1
         
        
    database = MySQLdb.connect(host = "127.0.0.1", user="vexpi", passwd="vexpi", db="vexpi")
    cur = database.cursor()
    
    cur.execute("SELECT * FROM Trainobservation WHERE pubDate BETWEEN '{0}' AND '{1}'".format(dateBegin,dateEnd))
    rows = cur.fetchall()
    
    if len(rows)==0:
        #nothing to update, return
        cur.close()
        database.close()
        return
    
    # process observation data into traintrackdata objects
    ttdata = []
    for r in rows:
        guid = r[0]
        pubDate = r[1]
        speed = r[2]
        lateness = r[4]
        trackid = r[7]
        
        l = [t for t in ttdata if t.guid==guid and t.trackid == trackid]
        if len(l)==0:
            # add new traintrackdata object
            t = TrainTrackData(guid, trackid)
            t.addData(pubDate, lateness, speed)
            ttdata.append(t)
        else:
            t=l[0]
            t.addData(pubDate, lateness, speed)
    
    # collect processed data and save into database
    for t in ttdata:
        (obsc, lateavg, latemedian, latemax) = t.getLateness()
        (obscspd, spdavg, spdmedian, spdmax) = t.getSpeed()
        
        # check database for existing entry
        cur.execute("SELECT * FROM Analyzeddata_month WHERE guid='{0}' AND track={1} AND month={2} AND year={3}".format(t.guid,t.trackid,month,year))
        entries = cur.fetchall()
        if len(entries)>0:
            savedObsCount, slAvg, slMax, sspdAvg, sspdMax = entries[0][4:9]
            # update data entry
            newCount = savedObsCount+obsc
            lateavg = (lateavg*obsc + slAvg*savedObsCount)/float(newCount)
            if slMax>latemax:
                latemax = slMax
            spdavg = (spdavg*obsc + sspdAvg*savedObsCount)/float(newCount)
            if sspdMax > spdmax:
                spdmax=sspdMax
            
            sqlstr = """UPDATE Analyzeddata_month
                        SET observationCount={0}, latenessAverage={1}, latenessMax={2}, speedAverage={3}, speedMax={4}
                        WHERE guid='{5}' AND track={6} AND month={7} AND year={8}""".format(newCount, lateavg,latemax,spdavg,spdmax,t.guid,t.trackid,month,year)
            cur.execute(sqlstr)
        else:
            # create new data entry
            sqlstr = "INSERT INTO Analyzeddata_month VALUES ('{0}',{1},{2},{3},{4},{5},{6},{7},{8})".format(t.guid, t.trackid, month, year, obsc, lateavg,latemax,spdavg,spdmax)
            cur.execute(sqlstr)
    
    cur.close()
    database.commit()
    database.close()

def calculateRecent(obsoleteDate):
    '''
    Updates table Analyzeddata_recent with the data from all entries in Trainobservation,
    where Trainobservation.pubDate is after the given 'obsoleteDate'.
    All data with pubDate prior to obsoleteDate will be deleted from Trainobservation.
    '''
    database = MySQLdb.connect(host = "127.0.0.1", user="vexpi", passwd="vexpi", db="vexpi")
    cur = database.cursor()
    
    # check when data has last been analyzed
    cur.execute("SELECT * FROM Analysisdates")
    updrow = cur.fetchall()
    saveddatadate = datetime.datetime(2000,1,1)
    if len(updrow)>0:
        # data has been updated at least once -> fetch all data since last update 
        saveddatadate = updrow[0][2] # column 2: updatedDate
        sqlstr = "SELECT * FROM Trainobservation WHERE pubDate > '{0}'".format(saveddatadate)
        cur.execute(sqlstr)
    else:
        # no data has been analyzed, do initial analysis
        cur.execute("SELECT * FROM Trainobservation") 
    
    #clear previous data
    sqlstr = "DELETE FROM Trainobservation WHERE pubDate < '{0}'".format(obsoleteDate)
    cur.execute(sqlstr)
    cur.execute("DELETE FROM Analyzeddata_recent")
    
    # fetch all recent data
    sqlstr = "SELECT * FROM Trainobservation"
    cur.execute(sqlstr)
    rows = cur.fetchall()
    
    # process observation data into traintrackdata objects
    ttdata = []
    for r in rows:
        guid = r[0]
        pubDate = r[1]
        speed = r[2]
        lateness = r[4]
        trackid = r[7]
        
        l = [t for t in ttdata if t.guid==guid and t.trackid == trackid]
        if len(l)==0:
            # add new traintrackdata object
            t = TrainTrackData(guid, trackid)
            t.addData(pubDate, lateness, speed)
            ttdata.append(t)
        else:
            t=l[0]
            t.addData(pubDate, lateness, speed)
        
    # collect processed data and save into database
    for t in ttdata:
        (obsc, lateavg, latemedian, latemax) = t.getLateness()
        (obscspd, spdavg, spdmedian, spdmax) = t.getSpeed()
        sqlstr = "INSERT INTO Analyzeddata_recent VALUES ('{0}',{1},{2},{3},{4},{5},{6},{7},{8})".format(t.guid, t.trackid, obsc, lateavg,latemedian,latemax,spdavg,spdmedian,spdmax)
        cur.execute(sqlstr) 
    
    cur.close()
    database.commit()
    database.close()
        

def calculateData(dataStoreTime):
    '''
    Updates the following tables using the data in Trainobservation:
    Analyzeddata_alltime - Any observations since last update time will be added
    Analyzeddata_month - Any observations since last update time will be added
    Analyzeddata_week - Any week whose beginning is in Trainobservation will be updated
    Analyzeddata_recent - Observations within 'dataStoreTime' of the latest observation in Trainobservation  will be compiled
    
    dataStoreTime - datetime.timedelta object that gives the length of time to keep in Trainobservation.
    Example: 
    If latest Trainobservation has pubDate 2013-04-16 12:05:04 and dataStoreTime = timedelta(days=7),
    then all observations with pubDate < 2013-04-09 12:05:04 will be removed from Trainobservation.
    '''
    # get time of last update for alltime and month
    database = MySQLdb.connect(host = "127.0.0.1", user="vexpi", passwd="vexpi", db="vexpi")
    cur = database.cursor()
    
    # get range of pubDates in Trainobservation
    cur.execute("SELECT MIN(pubDate) FROM Trainobservation")
    rows = cur.fetchall()
    if len(rows)==0:
        # no data in database, ABORT!
        cur.close()
        database.close()
        print "Database does not contain any data."
        return
    mindate = rows[0][0]
    cur.execute("SELECT MAX(pubDate) FROM Trainobservation")
    rows = cur.fetchall()
    maxdate = rows[0][0]
    
    cur.execute("SELECT * FROM Analysisdates")
    rows = cur.fetchall()
    if len(rows)==0:
        # no previous update exists, initialize update information
        print "First update initialization."
        updatedDate = mindate
        cur.execute("INSERT INTO Analysisdates VALUES (1, '{0}', '{0}')".format(updatedDate))
    else:
        updatedDate = rows[0][2]
    cur.close()
    database.commit()
    database.close()
    
    # update alltime data
    print "Updating alltime."
    calculateAllTime(updatedDate)
    
    # update month data for relevant months
    month = mindate.month
    year = mindate.year
    while datetime.datetime(year,month,1) <= maxdate:
        print "Updating month {0:02d}-{1}.".format(month, year)
        calculateMonth(year, month, updatedDate)
        month+=1
        if month>12:
            month-=12
            year+=1
    
    # update week data for relevant weeks
    weekLength = datetime.timedelta(weeks=1)
    weekBegin = mindate - datetime.timedelta(days = mindate.weekday(), hours = mindate.hour, minutes = mindate.minute, seconds = mindate.second)
    weekEnd = weekBegin + weekLength
    if weekBegin < mindate:
        weekBegin = weekEnd
        weekEnd = weekEnd + weekLength
    while weekBegin <= maxdate:
        print "Updating week of {0}.".format(weekBegin.date())
        calculateWeekOfDate(weekBegin.date())
        weekBegin = weekEnd
        weekEnd = weekEnd + weekLength
    
    # update recent data (and clear obsolete data)
    obsoleteDate = maxdate - dataStoreTime
    print "Updating recent."
    calculateRecent(obsoleteDate)
    
    # set last time of update to maxdate
    database = MySQLdb.connect(host = "127.0.0.1", user="vexpi", passwd="vexpi", db="vexpi")
    cur = database.cursor()
    cur.execute("UPDATE Analysisdates SET updatedDate='{0}' WHERE id=1".format(maxdate))
    cur.close()
    database.commit()
    database.close()
    
    print "Data up to date."
    

if __name__ == '__main__':
    storeTime = datetime.timedelta(weeks=2)
    calculateData(storeTime)
    
    
    