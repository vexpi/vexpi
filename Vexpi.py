#encoding=utf-8
'''
Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkil� Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html
'''


from analysis import Analysis
from Scrapper import TrainObservation
import threading

import Scrapper
import datetime
import time



def getData():
    '''
    Searches data using Scrapper, then moves it to Analysis, which saves the data into database in wanted form.
    '''
    while threadit.__len__() >0:
        if threadit[0].stopped == True:
            threadit.pop()
        time.sleep(2)
        
    a = threadit.append(threading.Timer(90.0, getData).start())
    print "Starting data gain"
    obslist = Scrapper.Scrape()
    print "{0}: scraped {1}".format(datetime.datetime.today(), len(obslist))
    analysis.analyze(obslist)
    analysis.saveToDatabase()
    print "Data gain complete"
    threadit.remove(a)


if __name__ == '__main__':
    '''
    Main entrance to Vexpi system.
    '''
    analysis = Analysis()
    threadit = []
    getData()

            
