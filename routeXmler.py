#encoding=utf-8
'''
Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html
'''
import MySQLdb

def printRoutes():
    database = MySQLdb.connect(host = "127.0.0.1", user="vexpi", passwd="vexpi", db="vexpi")
    cur = database.cursor()
    
    f = open('mapdata.kml','w')
    
    f.write("""<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://earth.google.com/kml/2.1">

  <Document>
    <name>Vexpi trackmap</name>
    <description>Vexpin bisnesdataa</description>

    <Style id="blueLine">
      <LineStyle>
        <color>ffff0000</color>
        <width>4</width>
      </LineStyle>
    </Style>
    <Style id="redLine">
      <LineStyle>
        <color>ff0000ff</color>
        <width>4</width>
      </LineStyle>
    </Style>
    <Style id="greenLine">
      <LineStyle>
        <color>ff009900</color>
        <width>4</width>
      </LineStyle>
    </Style>
    <Style id="orangeLine">
      <LineStyle>
        <color>ff00ccff</color>
        <width>4</width>
      </LineStyle>
    </Style>
    <Style id="pinkLine">
      <LineStyle>
        <color>ffff33ff</color>
        <width>4</width>
      </LineStyle>
    </Style>
    <Style id="brownLine">
      <LineStyle>
        <color>ff66a1cc</color>
        <width>4</width>
      </LineStyle>
    </Style>
    <Style id="purpleLine">
      <LineStyle>
        <color>ffcc00cc</color>
        <width>4</width>
      </LineStyle>
    </Style>
    <Style id="yellowLine">
      <LineStyle>
        <color>ff61f2f2</color>
        <width>4</width>
      </LineStyle>
    </Style>
    """)
    
    cur.execute("SELECT * FROM Track")
    rows = cur.fetchall()
    for r in rows:
        ss = r[1]
        es = r[2]
        '''
        s2 = ""
        for i in ss:
            if ord(i) < 128:
                s2 += i
        ss = s2
        s2 = ""
        for i in es:
            if ord(i) < 128:
                s2 += i
        es = s2
        '''
        print "Writing data for track {0}-{1}".format(ss,es)
        trackid = r[0]
        
        f.write('<Placemark>\n')
        f.write('<name>{0} {1}</name>\n'.format(ss,es))
        #f.write('<styleUrl>#blueLine</styleUrl>\n')
        #f.write('<LineString>\n')
        #f.write('<altitudeMode>relative</altitudeMode>\n')
        #f.write('<coordinates>\n')
        
        cur.execute("SELECT * FROM Station WHERE stationCode='{0}'".format(r[1]))
        statrows = cur.fetchall()
        statrow=statrows[0]
        geo1=statrow[3]
        geo2=statrow[2]
        f.write('{0},{1}\n'.format(geo1,geo2))
        
        cur.execute("SELECT * FROM Route WHERE track={0} ORDER BY Route.index".format(trackid))
        routerows = cur.fetchall()
        for routeitem in routerows:
            if(routeitem[3] != geo1 or routeitem[2] != geo2):
                geo1 = routeitem[3]
                geo2 = routeitem[2]
                f.write('{0},{1}\n'.format(geo1,geo2))
        
        cur.execute("SELECT * FROM Station WHERE stationCode='{0}'".format(r[2]))  
        statrows = cur.fetchall()
        statrow=statrows[0]
        if geo1!=statrow[3] or geo2!=statrow[2]:
            f.write('{0},{1}\n'.format(statrow[3],statrow[2]))  
        
            
        f.write('</coordinates>\n')
        #f.write('</LineString>\n')
        f.write('</Placemark>\n')
    
    f.write('</Document>\n')
    f.write('</kml>\n')
    f.close()
    cur.close()
    database.close()


if __name__ == '__main__':
    printRoutes();